/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.example.impl;

import java.util.Dictionary;

import org.amdatu.testing.example.Example;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

@Component(provides = {Example.class, ManagedService.class })
@Property(name = Constants.SERVICE_PID, value = "org.amdatu.testing.example")
public class ExampleComponent implements Example, ManagedService {
	@Inject
	private volatile BundleContext m_bundleContext;
	private volatile String m_message = "Hello";

	public String hello() {
		return m_message + " from " + m_bundleContext.getBundle().getSymbolicName();
	}

	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {

		if (properties != null) {
			String message = (String) properties.get("message");
			if (message != null) {
				m_message = message;
			}
		}
	}
}
