# Amdatu Testing

Amdatu Testing provides convenience utilities that simplify writing OSGi integration tests.

## Usage
Please visit the [Testing](http://www.amdatu.org/components/testing.html) component page on the Amdatu website for a detailed description on how to use Amdatu testing.

## Building

The source code can be built on the command line using Gradle, or by using [Bndtools](http://bndtools.org/) in Eclipse.
Tests can be ran from command line using Gradle as well, or by using JUnit in Eclipse.

### Eclipse using Bndtools
When Bndtools is correctly installed in Eclipse, import all subprojects into the workspace using `Import -> Existing Projects into Workspace` from the Eclipse context menu. Ensure `project -> Build Automatically` is checked in the top menu, and when no (compilation) errors are present, each subproject's bundles should be available in their `generated` folder.

###Gradle
When building from command line, invoke `./gradlew jar` from the root of the project. This will assemble all project bundles and place them in the `generated` folder of each subproject.


## Testing

### Eclipse using Bndtools
Unit tests can be ran per project or per class, using JUnit in Eclipse. Unit tests are ran by right clicking a project which contains a non-empty test folder or a test class and choosing `Run As -> JUnit Test`.

Integration tests can be ran in a similar way; right click an integration test project (commonly suffixed by .itest), but select `Run As -> Bnd OSGi Test Launcher (JUnit)` instead.

### Gradle
Unit tests are ran by invoking `./gradlew test` from the root of the project.

Integration tests can be ran by running a full build, this is done by invoking `./gradlew build` from the root of the project.
More info on available Gradle tasks can be found by invoking `./gradlew tasks`.

## Gradle license plugin

This project contains a Gradle license plugin which checks if all source files contain the correct license. It does so automatically when running a Gradle build from the commandline. This plugin can also be used to add missing headers to source files by running `./gradlew licenseFormat` from the root of the project. Please not though: when a different header is already present in a source file, the correct header as defined in the `etc` folder will be prefixed to the file with the already existing header, resulting in two headers! Please keep this in mind, and verify the output before committing after running this task.
	
## Links

* [Amdatu Website](http://amdatu.org/components/testing.html);
* [Source Code](https://bitbucket.org/amdatu/amdatu-testing);
* [Issue Tracking](https://amdatu.atlassian.net/browse/AMDATUTEST);
* [Development Wiki](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Testing);
* [Continuous Build](https://amdatu.atlassian.net/builds/browse/TST).

## License

The Amdatu Testing project is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

