/*
 * Copyright (c) 2010-2016 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator.test;

import org.amdatu.testing.configurator.ConfigurationStep;

/**
 * Configuration step for testing the configurationTimeout error message.
 */
public class DebugConfigurationStep implements ConfigurationStep {

    @Override
    public void apply(Object testCase) {
        // Do nothing
    }

    @Override
    public void cleanUp(Object testCase) {
        // Do nothing
    }

    @Override
    public String configurationTimeout(Object testCase) {
        return "\n== DEBUG STEP: \n This error is for testing and can not be resolved.";
    }
}
