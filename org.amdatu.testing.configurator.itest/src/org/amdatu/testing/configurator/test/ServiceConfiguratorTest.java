/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

public class ServiceConfiguratorTest {
    private static final int ENSURE_TIMEOUT = 15000;

    /**
     * Tests whether synchronous delivery works as expected.
     */
    @Test
    public void testSyncConfigurationDeliveryOk() throws Exception {
        String servicePid = "ServiceA";

        String[] ifaces = { ManagedService.class.getName(), Provider.class.getName() };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, servicePid);

        Provider service = new ConfigurableProvider();

        // We're *not* using the setup/teardown paradigm for this test case!
        BundleContext bc = FrameworkUtil.getBundle(getClass()).getBundleContext();
        ServiceRegistration<?> reg = bc.registerService(ifaces, service, props);

        Thread.sleep(1000);

        configure(this)
        	.add(createServiceDependency().setService(Provider.class).setRequired(true))
            .add(createConfiguration(servicePid).set("msg", "foo").setSynchronousDelivery(true))
            .apply();

        // Message should be adapted immediately...
        assertEquals("foo", service.getMessage());

        cleanUp(this);

        // Default value should be set immediately...
        assertEquals("default", service.getMessage());

        reg.unregister();
    }

    /**
     * Tests whether asynchronous delivery works as expected.
     */
    @Test
    public void testAsyncConfigurationDeliveryOK() {
        String servicePid = "ServiceB";

        String[] ifaces = { ManagedService.class.getName(), Provider.class.getName() };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, servicePid);
        
        Ensure configEnsure = new Ensure();
        Ensure deleteEnsure = new Ensure();
        
        ConfigurableEnsuredService service = new ConfigurableEnsuredService(deleteEnsure, configEnsure);

        // We're *not* using the setup/teardown paradigm for this test case!
        BundleContext bc = FrameworkUtil.getBundle(getClass()).getBundleContext();
        ServiceRegistration<?> reg = bc.registerService(ifaces, service, props);

        // Wait for null config to be provisioned
        deleteEnsure.waitForStep(1, ENSURE_TIMEOUT);
        
        configure(this)
        	
            .add(createConfiguration(servicePid).set("msg", "bar").setSynchronousDelivery(false))
            .apply();
        // Wait for actual config to be provisioned
        configEnsure.waitForStep(1, ENSURE_TIMEOUT);
        assertEquals("bar", service.getMessage());
        
        cleanUp(this);
        // Wait for null to be provisioned after cleanup
        deleteEnsure.waitForStep(2, ENSURE_TIMEOUT);
        assertEquals("default", service.getMessage());
        
        reg.unregister();
    }

    /**
     * Tests the asynchronous delivery of factory configurations works as expected.
     */
    @Test
    public void testAsyncFactoryDeliveryOK() {
        String servicePid = "ServiceC";

        String[] ifaces = { ManagedServiceFactory.class.getName() };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, servicePid);
        
        Ensure configEnsure = new Ensure();
        Ensure deleteEnsure = new Ensure();
        
        ProviderFactoryEnsured service = new ProviderFactoryEnsured(configEnsure, deleteEnsure);
        // We're *not* using the setup/teardown paradigm for this test case!
        BundleContext bc = FrameworkUtil.getBundle(getClass()).getBundleContext();
        ServiceRegistration<?> reg = bc.registerService(ifaces, service, props);
        
        configure(this)
            .add(createFactoryConfiguration(servicePid).set("msg", "qux").setSynchronousDelivery(false))
            .apply();
        
        // Wait for config to be provisioned to ensure provider is added
        configEnsure.waitForStep(1, ENSURE_TIMEOUT);
        assertEquals(1, service.m_providers.size());
        
        cleanUp(this);
        
        // Wait for provider to be removed
        deleteEnsure.waitForStep(1, ENSURE_TIMEOUT);
        assertEquals(0, service.m_providers.size());
        
        reg.unregister();
    }
    
    /**
     * Tests the synchronous delivery of factory configurations works as expected.
     */
    @Test
    public void testSyncFactoryDeliveryOk() throws Exception {
        String servicePid = "ServiceC";

        String[] ifaces = { ManagedServiceFactory.class.getName() };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, servicePid);

        ProviderFactory service = new ProviderFactory();

        // We're *not* using the setup/teardown paradigm for this test case!
        BundleContext bc = FrameworkUtil.getBundle(getClass()).getBundleContext();
        ServiceRegistration<?> reg = bc.registerService(ifaces, service, props);

        configure(this)
            .add(createFactoryConfiguration(servicePid).set("msg", "bar").setSynchronousDelivery(true))
            .apply();

        assertEquals(1, service.m_providers.size());

        cleanUp(this);

        assertEquals(0, service.m_providers.size());

        reg.unregister();
    }

    static class ProviderFactory implements ManagedServiceFactory {
        private final ConcurrentMap<String, Provider> m_providers = new ConcurrentHashMap<>(2);

        @Override
        public String getName() {
            return getClass().getName();
        }

        @Override
        public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
            String msg = "default";
            if (properties != null) {
                m_providers.putIfAbsent(pid, new ProviderImpl(msg));
            }
        }

        @Override
        public void deleted(String pid) {
            m_providers.remove(pid);
        }
    }

    /**
     * Configurable implementation of {@link Provider}.
     */
    static class ConfigurableProvider implements Provider, ManagedService {
        private volatile String m_msg = "default";

        @Override
        public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        	if (properties != null) {
                m_msg = (String) properties.get("msg");
            } else {
                m_msg = "default";
            }
        }

        @Override
        public String getMessage() {
            return m_msg;
        }
    }
    
    static class ProviderFactoryEnsured implements ManagedServiceFactory {
        private final Ensure m_delete_ensure;
        private final Ensure m_configured_ensure;
        private final ConcurrentMap<String, Provider> m_providers = new ConcurrentHashMap<>(2);

        public ProviderFactoryEnsured(Ensure configEnsure, Ensure deleteEnsure) {
            m_configured_ensure = configEnsure;
            m_delete_ensure = deleteEnsure;
        }
        
        @Override
        public String getName() {
            return getClass().getName();
        }

        @Override
        public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
            String msg = "default";
            if (properties != null) {
                msg = (String) properties.get("msg");
            }

            if (m_providers.putIfAbsent(pid, new ProviderImpl(msg)) == null) {
                m_configured_ensure.step(1);
            }
        }

        @Override
        public void deleted(String pid) {
            if (m_providers.remove(pid) != null) {
                m_delete_ensure.step(1);
            }
        }
    }
    
    /**
     * Configurable implementation of {@link Provider}.
     */
    static class ConfigurableEnsuredService implements Provider, ManagedService {
        private final Ensure m_delete_ensure;
        private final Ensure m_configured_ensure;
        private volatile String m_msg = "default";
        
        public ConfigurableEnsuredService(Ensure deleteEnsure, Ensure configEnsure) {
            m_delete_ensure = deleteEnsure;
            m_configured_ensure = configEnsure;
        }
        
        @Override
        public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
            if (properties != null) {
                m_msg = (String) properties.get("msg");
                m_configured_ensure.step(1);
            } else {
                m_msg = "default";
                m_delete_ensure.step(1);
            }
        }

        @Override
        public String getMessage() {
            return m_msg;
        }
    }
    
}