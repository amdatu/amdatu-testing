/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Proxy;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.amdatu.testing.configurator.InjectionFailedException;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentState;
import org.apache.felix.dm.ComponentStateListener;
import org.apache.felix.dm.DependencyManager;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;

public class DependencyTest {
    // Injected by Felix DM (as part of our test setup).
    private volatile BundleContext m_context;
    private volatile Provider m_provider;
    private volatile DependencyManager m_dependencyManager;
    // These two should *not* be managed by Felix DM (we're explicitly disallowing this)...
    private volatile Component m_component;
    private volatile ServiceRegistration<?> m_serviceRegistration;
    
    /**
     * Test whether the test case can handle service dynamics through the use of the track() configuration step.
     */
    @Test
    public void testServiceInjectionWithDynamicsOk() throws Exception {
        TestComponentStateListener listener = new TestComponentStateListener();
        
        // We're configuring the test *not* by the standard setup/teardown paradigm! Hence we need to manually create a DM instance...
        DependencyManager dm = new DependencyManager(FrameworkUtil.getBundle(getClass()).getBundleContext());

        Component c = dm.createComponent()
            .setInterface(Provider.class.getName(), null)
            .setImplementation(ProviderImpl.class)
            .add(listener);

        configure(this)
            .add(createServiceDependency().setService(Provider.class).setRequired(false))
            .apply();

        // check that autoConfig works as configured
        assertNull(m_component);
        assertNull(m_serviceRegistration);
        assertNotNull(m_context);
        assertNotNull(m_dependencyManager);
        
        dm.add(c);

        assertTrue(listener.m_startLatch.await(5, TimeUnit.SECONDS));
        assertNotNull(m_provider);
        assertFalse("Did not expect a NullObject", Proxy.isProxyClass(m_provider.getClass()));

        dm.remove(c);

        assertTrue(listener.m_stopLatch.await(5, TimeUnit.SECONDS));
        assertNotNull(m_provider);
        assertTrue("Expected a NullObject", Proxy.isProxyClass(m_provider.getClass()));

        cleanUp(this);
    }

    /**
     * Test whether the test case can handle service dynamics through the use of the track() configuration step.
     */
    @Test
    public void testRequiredServiceInjectionOk() {
        // We're configuring the test *not* by the standard setup/teardown paradigm! Hence we need to manually create a DM instance...
        DependencyManager dm = new DependencyManager(FrameworkUtil.getBundle(getClass()).getBundleContext());

        try {
            // This should fail, as Provider is not available as service...
            configure(this)
                .setTimeout(500, TimeUnit.MILLISECONDS)
                .add(createServiceDependency().setService(Provider.class).setRequired(true))
                .add(new DebugConfigurationStep())
                .apply();

            fail("InjectionFailedException expected!");
        } catch (InjectionFailedException e) {
            e.printStackTrace();
            assertTrue(e.getMessage().contains("This error is for testing and can not be resolved."));

            // Ok; expected...
            cleanUp(this);
        }

        Component c = dm.createComponent()
                        .setInterface(Provider.class.getName(), null)
                        .setImplementation(ProviderImpl.class);

        dm.add(c);

        try {
            configure(this)
                .setTimeout(500, TimeUnit.MILLISECONDS)
                .add(createServiceDependency().setService(Provider.class).setRequired(true))
                .apply();

            assertNotNull("Required provider service should be injected!", m_provider);
        } finally {
            dm.remove(c);

            cleanUp(this);
        }
    }

    static class TestComponentStateListener implements ComponentStateListener {
        final CountDownLatch m_startLatch = new CountDownLatch(1);
        final CountDownLatch m_stopLatch = new CountDownLatch(1);

        @Override
        public void changed(Component c, ComponentState state) {
            switch (state) {
                case INACTIVE:
                    m_stopLatch.countDown();
                    break;
                case TRACKING_OPTIONAL:
                    m_startLatch.countDown();
                    break;
                default:
            }
        }
    }
}
