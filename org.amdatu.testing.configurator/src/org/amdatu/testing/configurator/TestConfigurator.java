/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import java.util.Objects;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ConfigurationDependency;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.ServiceDependency;

/**
 * Utilities for helping out with common OSGi integration test operations such as configuring and retrieving service instances.
 * 
 * Usage:
 * 
 * <ul>
 * <li> Make sure your test case inherits from JUnit's <code>TestCase</code> class.</li>
 * <li> Import the public methods defined in this class statically to your test case </li>
 * <li> Define a private volatile field for each service you want to inject to your test </li>
 * <li> Override the setUp() method, and do the necessary configurations. </li>
 * <li> Override the tearDown() method and call the static cleanUpTest() method.
 * This will revert any persistent changes done by any of the configuration steps. </li>
 * </ul>
 * Here is an example of configuring a service called MyService that uses the Amdatu MongoDB service:
 * 
 * </ul>
 * 
 * <pre>
 * <code>
 * import static org.amdatu.testing.configurator.TestConfigurator.*;
 * // ...
 * 
 * public class MyTestCase extends TestCase {
 *   // injected automatically through Felix DependencyManager
 *   private volatile MyService myService;
 * 
 *   public void setUp() {
 *      configure(this)
 *         .add(configureFactory("org.amdatu.mongo").set("dbName", "test"))
 *         .add(createServiceDependency().setService(MyService.class))
 *         .apply();
 *   }
 *
 *   public void tearDown() {
 *      cleanUp(this);
 *   }
 *
 *   {@literal @Test}
 *   public void testSomething() {
 *      assertNotNull(myService);
 *   }
 * }
 * </code>
 * </pre>
 */
public final class TestConfigurator {
    private static final ThreadLocal<TestConfiguration> CONFIGS = new ThreadLocal<>();

    private TestConfigurator() {
        // Avoid instances being made...
    }

    /**
     * Sets up a JUnit test case, performing all specified configurations and registering
     * all necessary services.
     * 
     * @return the container of configuration steps.
     */
    public static TestConfiguration configure(Object testCase) {
        TestConfiguration container = CONFIGS.get();
        if (container != null) {
            throw new RuntimeException(
                "Multiple test configurations not supported! Did you forget to invoke org.amdatu.testing.configurator.TestConfigurator.cleanUp(this) after your test?");
        }
        container = new TestConfiguration(testCase);
        CONFIGS.set(container);
        return container;
    }

    /**
     * @param testCase the test case to clean up, cannot be <code>null</code>.
     */
    public static void cleanUp(Object testCase) {
        TestConfiguration container = CONFIGS.get();
        if (container != null) {
            try {
                container.cleanUp();
            } finally {
                CONFIGS.remove();
            }
        }
    }

    /**
     * Configures the specified service factory.
     * 
     * @param pid The pid of the service factory to configure
     * @return ServiceConfigurator instance used to set configuration properties.
     */
    public static ServiceConfigurator createFactoryConfiguration(String pid) {
        return ServiceConfigurator.createFactoryServiceConfigurator(pid);
    }

    /**
     * Configures a service with the specified pid.
     * 
     * @param pid The pid of the service to configure.
     * @return ServiceConfigurator instance used to set configuration properties.
     */
    public static ServiceConfigurator createConfiguration(String pid) {
        return ServiceConfigurator.createServiceConfigurator(pid);
    }

    /**
     * Creates a new {@link Component} instance, for adding custom service implementations at runtime.
     * 
     * @return an empty {@link Component} instance, never <code>null</code>.
     */
    public static Component createComponent() {
        TestConfiguration container = getContainer();
        return container.getDependencyManager().createComponent();
    }

    /**
     * Creates a new {@link ConfigurationDependency} instance, to add to custom service implementations at runtime.
     * 
     * @return an empty {@link ConfigurationDependency} instance, never <code>null</code>.
     */
    public static ConfigurationDependency createConfigurationDependency() {
        TestConfiguration container = getContainer();
        return container.getDependencyManager().createConfigurationDependency();
    }

    /**
     * Injects an empty, unconfigured, service dependency.
     * 
     * @return a {@link ServiceDependency} builder allowing you to customize the injection further.
     */
    public static ServiceDependency createServiceDependency() {
        TestConfiguration container = getContainer();
        return container.getDependencyManager().createServiceDependency();
    }

    /**
     * Return t he <code>DependencyMangaer</code> used by the test associated with the current thread.
     */
    public static DependencyManager getDependencyManager() {
        return getContainer().getDependencyManager();
    }

    static TestConfiguration getContainer() {
        TestConfiguration result = CONFIGS.get();
        Objects.requireNonNull(result, "No test-step container found; is it configured properly?!");
        return result;
    }

    static ConfigurationStep wrap(final Object step) {
        Objects.requireNonNull(step, "Step cannot be null!");
        if (step instanceof Component) {
            return wrap((Component) step);
        } else if (step instanceof ServiceDependency) {
            return wrap((ServiceDependency) step);
        } else if (step instanceof ConfigurationStep) {
            return (ConfigurationStep) step;
        } else {
            throw new IllegalArgumentException("Unable to wrap: " + step.getClass().getSimpleName() + "!");
        }
    }

    private static ConfigurationStep wrap(final Component component) {
        return new ConfigurationStep() {
            @Override
            public void apply(Object testCase) {
                getContainer().getDependencyManager().add(component);
            }

            @Override
            public void cleanUp(Object testCase) {
                getContainer().getDependencyManager().remove(component);
            }
        };
    }

    private static ConfigurationStep wrap(final ServiceDependency dependency) {
        return new ConfigurationStep() {
            @Override
            public void apply(Object testCase) {
                getContainer().getComponent().add(dependency);
            }

            @Override
            public void cleanUp(Object testCase) {
                getContainer().getComponent().remove(dependency);
            }
        };
    }
}
