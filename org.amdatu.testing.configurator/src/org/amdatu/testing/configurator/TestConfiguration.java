/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import static org.amdatu.testing.configurator.TestConfigurator.wrap;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentDeclaration;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.ServiceDependency;
import org.apache.felix.dm.diagnostics.CircularDependency;
import org.apache.felix.dm.diagnostics.DependencyGraph;
import org.apache.felix.dm.diagnostics.DependencyGraph.ComponentState;
import org.apache.felix.dm.diagnostics.DependencyGraph.DependencyState;
import org.apache.felix.dm.diagnostics.MissingDependency;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;

public class TestConfiguration {
    private final Object m_testCase;
    private final BundleContext m_context;
    private final List<ConfigurationStep> m_steps;
    private final DependencyManager m_dm;

    private Component m_component;
    private long m_timeout;

    TestConfiguration(Object testCase) {
        Objects.requireNonNull(testCase, "TestCase cannot be null!");
        m_testCase = testCase;

        Bundle bundle = FrameworkUtil.getBundle(m_testCase.getClass());
        Objects.requireNonNull(bundle, "Not running as OSGi test case?!");

        m_context = bundle.getBundleContext();
        m_dm = new DependencyManager(m_context);

        m_steps = new ArrayList<>();

        m_component = m_dm.createComponent().setImplementation(m_testCase);
        m_component.setAutoConfig(Component.class, false);
        m_component.setAutoConfig(ServiceRegistration.class, false);

        m_timeout = TimeUnit.SECONDS.toMillis(5L);
    }

    /**
     * Adds a given {@link Component} to this container.
     *
     * @param component the component to add, cannot be <code>null</code>.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration add(Component component) {
        Objects.requireNonNull(component, "Component cannot be null!");
        m_steps.add(wrap(component));
        return this;
    }

    /**
     * Adds a given {@link ConfigurationStep} to this container.
     *
     * @param step the step to add, cannot be <code>null</code>.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration add(ConfigurationStep step) {
        Objects.requireNonNull(step, "Step cannot be null!");
        m_steps.add(step);
        return this;
    }

    /**
     * Adds a given {@link ServiceDependency} to this container.
     *
     * @param dependency the service dependency to add, cannot be <code>null</code>.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration add(ServiceDependency dependency) {
        Objects.requireNonNull(dependency, "ServiceDependency cannot be null!");
        m_steps.add(wrap(dependency));
        return this;
    }

    /**
     * Applies this configuration to the current test case.
     */
    public void apply() {
        try {
            for (ConfigurationStep step : m_steps) {
                step.apply(m_testCase);
            }

            CountDownLatch latch = new CountDownLatch(1);
            m_component.add((comp, state) -> {
                if (state == org.apache.felix.dm.ComponentState.TRACKING_OPTIONAL) {
                    latch.countDown();
                }
            });

            m_dm.add(m_component);

            if (!latch.await(m_timeout, TimeUnit.MILLISECONDS)) {
                throw new InjectionFailedException("Failed to configure test case!", describeInjectionFailure());
            }
        } catch (InterruptedException e) {
            try {
                TestConfigurator.cleanUp(m_testCase);
            } catch (RuntimeException cleanUpExc) {
                e.addSuppressed(cleanUpExc);
            }
            throw new InjectionFailedException("Failed to configure test case: interrupted by another thread!", e);
        } catch (RuntimeException e) {
            try {
                TestConfigurator.cleanUp(m_testCase);
            } catch (RuntimeException cleanUpExc) {
                e.addSuppressed(cleanUpExc);
            }
            throw e;
        }
    }

    /**
     * Applies a clean up to the current test case.
     */
    public void cleanUp() {
        try {
            for (ConfigurationStep step : m_steps) {
                step.cleanUp(m_testCase);
            }
        } finally {
            m_dm.remove(m_component);
            m_component = null;
            m_steps.clear();
        }
    }

    /**
     * @param clazz      the type to include/exclude from being auto-injected in the test-case;
     * @param autoConfig <code>true</code> if the type should by auto-injected, <code>false</code> if it should be omitted.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration setAutoConfig(Class<?> clazz, boolean autoConfig) {
        m_component.setAutoConfig(clazz, autoConfig);
        return this;
    }

    /**
     * Sets the timeout to wait until all dependencies are satisfied.
     *
     * @param duration the duration of the timeout;
     * @param unit     the unit of time.
     * @return this container, never <code>null</code>.
     */
    public TestConfiguration setTimeout(long duration, TimeUnit unit) {
        m_timeout = unit.toMillis(duration);
        return this;
    }

    final Component getComponent() {
        return m_component;
    }

    final DependencyManager getDependencyManager() {
        return m_dm;
    }

    final Object getTestCase() {
        return m_testCase;
    }

    private void describeCircularDependencies(StringBuilder sb, List<CircularDependency> circularDependencies) {
        if (circularDependencies.isEmpty()) {
            return;
        }
        sb.append("- The following dependency cycles were detected:\n");
        int no = 1;
        for (CircularDependency cd : circularDependencies) {
            sb.append("  * ").append(no++).append(":");
            for (ComponentDeclaration c : cd.getComponents()) {
                sb.append(" -> ").append(c.getName());
            }
            sb.append("\n");
        }
    }

    /**
     * @return a description of all dependency problems that caused the injection to fail as string similar to the "dm wtf" command.
     */
    private String describeInjectionFailure() {
        // AMDATUTEST-25 - use the utilities provided by DM for describing the problems...
        DependencyGraph graph = DependencyGraph.getGraph(ComponentState.UNREGISTERED, DependencyState.REQUIRED_UNAVAILABLE);

        StringBuilder sb = new StringBuilder();
        describeCircularDependencies(sb, graph.getCircularDependencies());
        describeMissingDependencies(sb, graph, "configuration");
        describeMissingDependencies(sb, graph, "service");
        describeMissingDependencies(sb, graph, "resource");
        describeMissingDependencies(sb, graph, "bundle");

        for (ConfigurationStep step : m_steps) {
            String stepDescription = step.configurationTimeout(m_testCase);
            if (stepDescription != null) {
                sb.append("\n- ").append(step).append(":\n");
                sb.append(stepDescription);
                sb.append("\n");
            }
        }

        return sb.toString().trim();
    }

    private void describeMissingDependencies(StringBuilder sb, DependencyGraph graph, String missingType) {
        List<MissingDependency> deps = graph.getMissingDependencies(missingType);
        if (deps.isEmpty()) {
            return;
        }
        sb.append("- The following ").append(missingType).append(" dependencies were missing:\n");
        for (MissingDependency md : deps) {
            sb.append("  * ").append(md.getName()).append(" for bundle ").append(md.getBundleName()).append("\n");
        }
    }
}
