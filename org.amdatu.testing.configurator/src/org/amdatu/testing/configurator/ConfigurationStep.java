/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

/**
 * Represents a single configuration step that does "something" during the setup or teardown of each test case.
 */
public interface ConfigurationStep {

    /**
     * Called as part of the setup of each test case.
     * <p>
     * All configuration steps are applied in a best effort approach: all steps are expected to
     * succeed. If a step is not able to apply properly, it <em>may</em> throw an
     * {@link TestConfigurationException} (or a subclass) to indicate this. The preceeding steps
     * are <b>not</b> reverted in that case, but {@link #cleanUp(Object)} is guaranteed to be
     * called.
     * </p>
     *
     * @param testCase the test case to cleanup.
     * @throws TestConfigurationException in case of an fatal error while applying this configuration step.
     */
    void apply(Object testCase);

    /**
     * Called as part of the teardown of each test case.
     * <p>
     * Implementation should be aware that this method is called while {@link #apply(Object)} is
     * not! If an implementation keeps state, it cannot assume this state is valid when this method
     * is called.
     * </p>
     * <p>
     * Implementation should <b>not</b> throw <b>any</b> exceptions from this method.
     * </p>
     *
     * @param testCase the test case to cleanup.
     */
    void cleanUp(Object testCase);

    /**
     * Called before teardown in case test configuration failed with a timeout.
     * <p>
     * Implementation can be used to contribute information that could help finding the cause of the timeout.
     * </p>
     * <p>
     * Implementation should <b>not</b> throw <b>any</b> exceptions from this method.
     * </p>
     * @param testCase the test case
     * @return contribution to the test failure information
     */
    default String configurationTimeout(Object testCase) {
        return null;
    }
}
