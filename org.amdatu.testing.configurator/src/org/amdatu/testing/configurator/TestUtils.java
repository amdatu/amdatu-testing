/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.ConfigurationListener;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides several utility methods to work with OSGi services in integration tests.
 */
public class TestUtils {
    private static final int WAIT_FOR_SERVICE_SEC = 5;

    public static void deleteConfiguration(String servicePid) {
        ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
        try {
            BundleContext context = getBundleContext();

            final CountDownLatch deleteLatch = new CountDownLatch(1);
            ConfigurationListener listener = new ConfigurationListener() {
                @Override
                public void configurationEvent(ConfigurationEvent event) {
                    if (event.getPid().equals(servicePid) && event.getType() == ConfigurationEvent.CM_DELETED) {
                        deleteLatch.countDown();
                    }
                }
            };

            ServiceRegistration<?> registration = context.registerService(ConfigurationListener.class, listener, null);
            Configuration configuration = configurationAdmin.getConfiguration(servicePid, null);
            configuration.delete();

            boolean success = deleteLatch.await(2, TimeUnit.SECONDS);
            registration.unregister();

            if (!success) {
                throw new RuntimeException("Waiting too long for deletion of configuration " + servicePid);
            }
        } catch (IOException | InterruptedException ex) {
            unchecked(ex);
        }
    }

    public static Configuration getConfiguration(String servicePid) {
        ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
        try {
            return configurationAdmin.getConfiguration(servicePid, null);
        } catch (IOException ex) {
            unchecked(ex);
            return null; // keeps compiler happy...
        }
    }

    public static Configuration getFactoryConfiguration(String factoryPid) {
        ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
        try {
            return configurationAdmin.createFactoryConfiguration(factoryPid, null);
        } catch (IOException ex) {
            unchecked(ex);
            return null; // keeps compiler happy...
        }
    }

    public static <T> T getService(Class<T> serviceClass) {
        return getService(serviceClass, null);
    }

    public static <T> T getService(Class<T> serviceClass, String filterString) {
        return getService(serviceClass, filterString, WAIT_FOR_SERVICE_SEC);
    }

    public static <T> T getService(Class<T> serviceClass, String filterString, long timeoutSeconds) {
        T serviceInstance = null;

        ServiceTracker<T, T> serviceTracker;
        BundleContext context = getBundleContext();

        if (filterString == null) {
            serviceTracker = new ServiceTracker<>(context, serviceClass.getName(), null);
        } else {
            try {
                filterString = String.format("(&(%s=%s)%s)", Constants.OBJECTCLASS, serviceClass.getName(), filterString);
                serviceTracker = new ServiceTracker<>(context, context.createFilter(filterString), null);
            } catch (InvalidSyntaxException e) {
                throw new RuntimeException("Invalid filter string: " + filterString, e);
            }
        }

        serviceTracker.open();

        try {
            serviceInstance = serviceTracker.waitForService(timeoutSeconds * 1000);
            if (serviceInstance == null) {
                throw new IllegalStateException(serviceClass + " instance not available within " + timeoutSeconds + " seconds");
            }
            return serviceInstance;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new IllegalStateException(serviceClass + " service is not available: interrupted while waiting!", e);
        }
    }

    public static BundleContext getBundleContext(Class<?> type) {
        Bundle bundle = FrameworkUtil.getBundle(type);
        Objects.requireNonNull(bundle, "No bundle available? Are we running in a OSGi test container?!");
        return bundle.getBundleContext();
    }

    static <T> T unchecked(Throwable throwable) {
        return TestUtils._unchecked(throwable);
    }

    @SuppressWarnings("unchecked")
    private static <T, E extends Throwable> T _unchecked(Throwable throwable) throws E {
        throw (E) throwable;
    }

    private static BundleContext getBundleContext() {
        return getBundleContext(TestUtils.class);
    }
}
