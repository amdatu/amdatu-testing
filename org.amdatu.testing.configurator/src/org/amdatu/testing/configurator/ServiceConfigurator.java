/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

import static org.amdatu.testing.configurator.TestUtils.*;
import static org.osgi.framework.Constants.OBJECTCLASS;
import static org.osgi.framework.Constants.SERVICE_PID;
import static org.osgi.framework.FrameworkUtil.createFilter;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Configuration step that configures a single OSGi service factory.
 */
public class ServiceConfigurator implements ConfigurationStep {
    /**
     * Configures a service asynchronously by using a {@link ConfigurationAdmin} service.
     */
    final class AsyncServiceConfigurator implements ConfigurationStep {
        @Override
        public void apply(Object testCase) {
            ConfigurationAdmin configurationAdmin = getService(ConfigurationAdmin.class);
            try {
                Configuration configuration;
                if (m_factory) {
                    configuration = configurationAdmin.createFactoryConfiguration(m_servicePid, null);
                } else {
                    configuration = configurationAdmin.getConfiguration(m_servicePid, null);
                }
                configuration.update(m_properties);

                m_provisionedPid = configuration.getPid();
            } catch (IOException ex) {
                unchecked(ex);
            }
        }

        @Override
        public void cleanUp(Object testCase) {
            if (m_provisionedPid != null) {
                deleteConfiguration(m_provisionedPid);
            }
        }
    }

    /**
     * Configures a service synchronously by directly invoking the registered {@link ManagedServiceFactory} or {@link ManagedService}.
     */
    final class SyncServiceConfigurator implements ConfigurationStep {
        private ServiceTracker<?, ?> m_tracker;

        @Override
        public void apply(Object testCase) {
            try {
                Filter filter = createServiceFilter();

                m_tracker = new ServiceTracker<>(getBundleContext(testCase.getClass()), filter, null);
                m_tracker.open();

                Object service = m_tracker.waitForService(m_timeout);
                if (service == null) {
                    throw new RuntimeException("Unable to find managed service " + (m_factory ? "factory " : "") + "with PID: " + m_servicePid);
                }

                if (m_factory) {
                    // Generate a particular PID for this factory...
                    String pid = UUID.randomUUID().toString();

                    ((ManagedServiceFactory) service).updated(pid, m_properties);

                    m_provisionedPid = pid;
                } else {
                    ((ManagedService) service).updated(m_properties);

                    m_provisionedPid = m_servicePid;
                }
            } catch (ConfigurationException | InvalidSyntaxException ex) {
                unchecked(ex);
            } catch (InterruptedException ex) {
                // Ok; we're stopping already...
            }
        }

        @Override
        public void cleanUp(Object testCase) {
            if (m_tracker == null) {
                // Apply hasn't been called...
                return;
            }

            try {
                Object service = m_tracker.waitForService(m_timeout);
                if (service == null) {
                    throw new RuntimeException("Unable to find service!");
                }

                if (m_factory) {
                    ((ManagedServiceFactory) service).deleted(m_provisionedPid);
                } else {
                    ((ManagedService) service).updated(null);
                }
            } catch (ConfigurationException ex) {
                unchecked(ex);
            } catch (InterruptedException ex) {
                // Ok; we're stopping already...
            } finally {
                m_tracker.close();
                m_tracker = null;
            }
        }

        private Filter createServiceFilter() throws InvalidSyntaxException {
            String type = (m_factory ? ManagedServiceFactory.class : ManagedService.class).getName();
            return createFilter(String.format("(&(%s=%s)(%s=%s))", OBJECTCLASS, type, SERVICE_PID, m_servicePid));
        }
    }

    private static final long DEFAULT_TIMEOUT = 5000; /* ms */

    private final Dictionary<String, Object> m_properties;
    private final String m_servicePid;
    private final boolean m_factory;

    private long m_timeout;
    private boolean m_synchronousDelivery;
    private String m_provisionedPid;
    private ConfigurationStep m_delegate;

    private ServiceConfigurator(String servicePid, boolean factory) {
        m_properties = new Hashtable<>();
        m_servicePid = servicePid;
        m_factory = factory;
        m_timeout = DEFAULT_TIMEOUT;
        m_synchronousDelivery = false;
        m_provisionedPid = null;
    }

    /**
     * @param servicePid the factory PID of the managed service factory to configure, cannot be <code>null</code>.
     * @return a new {@link ServiceConfigurator} instance that can be configured further, never <code>null</code>.
     */
    public static ServiceConfigurator createFactoryServiceConfigurator(String servicePid) {
        return new ServiceConfigurator(servicePid, true);
    }

    /**
     * @param servicePid the PID of the managed service to configure, cannot be <code>null</code>.
     * @return a new {@link ServiceConfigurator} instance that can be configured further, never <code>null</code>.
     */
    public static ServiceConfigurator createServiceConfigurator(String servicePid) {
        return new ServiceConfigurator(servicePid, false);
    }

    public void apply(Object testCase) {
        if (m_synchronousDelivery) {
            m_delegate = new SyncServiceConfigurator();
        } else {
            m_delegate = new AsyncServiceConfigurator();
        }
        m_delegate.apply(testCase);
    }

    public void cleanUp(Object testCase) {
        m_delegate.cleanUp(testCase);
    }

    /**
     * Adds a new key/value pair to the configuration of the managed service (factory).
     *
     * @param key the configuration key, cannot be <code>null</code>;
     * @param value the value to set, can be <code>null</code>.
     * @return this {@link ServiceConfigurator} instance.
     */
    public ServiceConfigurator set(String key, Object value) {
        Objects.requireNonNull(key, "Key cannot be null!");
        m_properties.put(key, value);
        return this;
    }

    /**
     * Adds new key/value pairs to the configuration of the managed service (factory).
     *
     * @param items the key/values to use as configuration, the number of provided items should be a multiple of two.
     * @return this {@link ServiceConfigurator} instance.
     * @throws IllegalArgumentException in case less than two items were supplied.
     */
    public ServiceConfigurator set(Object... items) {
        if (items.length < 2) {
            throw new IllegalArgumentException("Need at least two items!");
        }
        for (int i = 0; i < items.length; i += 2) {
            m_properties.put(items[i].toString(), items[i + 1]);
        }
        return this;
    }

    /**
     * Sets whether or not synchronous delivery should be used to supply the configuration to the managed service (factory).
     *
     * @param synchronousDelivery <code>true</code> if synchronous delivery should be used, <code>false</code> to use the asynchronous delivery.
     * @return this {@link ServiceConfigurator} instance.
     */
    public ServiceConfigurator setSynchronousDelivery(boolean synchronousDelivery) {
        m_synchronousDelivery = synchronousDelivery;
        return this;
    }

    /**
     * Sets the timeout to wait until the managed service (factory) is available in case synchronous configuration delivery is used.
     *
     * @param duration the duration to wait, &gt; 0;
     * @param unit the time unit, cannot be <code>null</code>.
     * @return this {@link ServiceConfigurator} instance.
     * @see #setSynchronousDelivery(boolean)
     */
    public ServiceConfigurator setTimeout(long duration, TimeUnit unit) {
        m_timeout = unit.toMillis(duration);
        return this;
    }
}
