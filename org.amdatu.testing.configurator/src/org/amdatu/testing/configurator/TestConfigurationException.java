/*
 * Copyright (c) 2010-2016 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

/**
 * General exception that can be thrown from configuration steps to indicate something went wrong.
 */
public class TestConfigurationException extends RuntimeException {
    private static final long serialVersionUID = -224005092228516000L;

    /**
     * Creates a new {@link TestConfigurationException} instance.
     */
    public TestConfigurationException() {
        super();
    }

    /**
     * Creates a new {@link TestConfigurationException} instance.
     */
    public TestConfigurationException(String message) {
        super(message);
    }

    /**
     * Creates a new {@link TestConfigurationException} instance.
     */
    public TestConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a new {@link TestConfigurationException} instance.
     */
    public TestConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * Creates a new {@link TestConfigurationException} instance.
     */
    public TestConfigurationException(Throwable cause) {
        super(cause);
    }

}
